/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printUserInfo() {

	let firstName = prompt("Enter Your First Name:");
	let lastName = prompt("Enter Your Last Name:");
	let age = prompt("Enter Your Age:");
    let location = prompt("Enter Your Location:");
	console.log("Hello, " + firstName  + " " + lastName );
	console.log("You are"+ " " + age + " " + " years old" )
	console.log("You live in " + location + " City")
	
}

printUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function display_Favebands() {

		let top_one = "1.Paramore ";
		let top_two = "2.Alesana ";
		let top_three = "3.Faberdrive ";
		let top_four = "4.Sleeping with Sirens ";
		let top_five= "5.Secondhand Serenade";
		
		console.log(top_one+ "\n" +top_two+ "\n" + top_three+ "\n" + top_four+ "\n" +top_five);
	}


	display_Favebands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


	
*/
	
	//third function here:
	function display_Favemovies() {

		let mtop_one = "1.Dead Poets Society ";
		let rtop_one="Rotten Tomatoes Rating : 95%";
		let mtop_two = "2.Raya ";
		let rtop_two="Rotten Tomatoes Rating : 94%";
		let mtop_three = "3.The Notebook ";
		let rtop_three="Rotten Tomatoes Rating : 93%";
		let mtop_four = "4.Kimetsu no yaiba ";
		let rtop_four="Rotten Tomatoes Rating : 92%";
		let mtop_five= "5.Darna";
		let rtop_five="Rotten Tomatoes Rating : 99%";
		
		console.log(mtop_one+ "\n" +rtop_one+ "\n" +mtop_two+ "\n" +rtop_two+ "\n" +mtop_three+ "\n" +rtop_three+ "\n" +mtop_four+ "\n" +rtop_four+ "\n" +mtop_five+ "\n"+rtop_five);
	}


	display_Favemovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers() {
// let printFriends()  
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);

printUsers();